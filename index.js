import path from 'path';
import express from 'express';
import favicon from 'express-favicon';
import open from 'open';
import webpack from 'webpack';
import morgan from 'morgan';
import winston from 'winston';
import configuration from './webpack.config';

import './dotenv';

const app = express();
const baseFile = path.join(__dirname, 'dist/index.html');
const compiler = webpack(configuration);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: configuration.output.publicPath,
}));
app.use(require('webpack-hot-middleware')(compiler));

app.use(favicon(path.join(__dirname, '/src/bullseye.png')));
app.use(morgan('dev'));
app.get('/', (req, res) => {
  res.sendFile(baseFile);
});

app.listen(process.env.PORT, (error) => {
  if (error) {
    winston.log(error);
  } else {
    open(`http://localhost:${process.env.PORT}`);
  }
});
