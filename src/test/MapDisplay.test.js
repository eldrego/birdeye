import React from 'react';
import { shallow } from 'enzyme';
import MapDisplay from '../components/MapDisplay.jsx';

const wrapper = shallow(<MapDisplay />);

describe('Component: App', () => {
  it('it renders 1 <MapDisplay /> component', () => {
    expect(wrapper).toHaveLength(1);
  });
});
