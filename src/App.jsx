import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Header from './components/common/Header';
import MapDisplay from './components/MapDisplay';
import Places from './components/Places';
import styles from './style.css';

export default class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <MapDisplay />
        <Places />
      </div>
    );
  }
}

ReactDom.render(<App />, document.getElementById('app'));
