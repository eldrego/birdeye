import React, { Component } from 'react';
import Map from './Map';

export default class MapDisplay extends Component {
  render() {
    const location = {
      lat: 6.59023,
      lng: 3.462037
    };
    return (
      <div className="map-content">
        <Map
          className="map"
          center={location}
          zoom={11}
          containerElement={<div style={{ height: '100%' }} />}
          mapElement={<div style={{ height: '100%' }} />}/>
      </div>
    );
  }
}
