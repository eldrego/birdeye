import React from 'react';
import PropTypes from 'prop-types';
import { withGoogleMap, GoogleMap } from 'react-google-maps';

/**
 * [defaultZoom description]
 * @type {Object}
 */
class Map extends React.Component {
  constructor() {
    super();
    this.state = {
      map: null,
    };

    this.mapLoaded = this.mapLoaded.bind(this);
    this.mapMoved = this.mapMoved.bind(this);
  }

  /**
   * [mapMoved description]
   */
  mapMoved() {
    // console.log(`map moved + ${JSON.stringify(this.state.map.getCenter())}`);
  }

  /**
   * [mapLoaded description]
   * @param  {[type]} map [description]
   */
  mapLoaded(map) {
    if (this.state.map != null) {
      return;
    }

    this.setState({
      map
    });
  }

  render() {
    return (
      <GoogleMap
        ref={this.mapLoaded}
        onDragEnd={this.mapMoved}
        defaultZoom={this.props.zoom}
        defaultCenter={this.props.center}
      />
    );
  }
}

Map.propTypes = {
  center: PropTypes.object,
  zoom: PropTypes.number
};

export default withGoogleMap(Map);
